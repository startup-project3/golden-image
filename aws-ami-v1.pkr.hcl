# packer puglin for AWS 
# https://www.packer.io/plugins/builders/amazon 
packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.3"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

# which ami to use as the base and where to save it
source "amazon-ebs" "ubuntu" {
  region          = "ap-south-1"
  ami_name        = "ami-ubuntu-{{timestamp}}"
  instance_type   = "t2.micro"
  source_ami      = "ami-03f4878755434977f"
  ssh_username    = "ubuntu"
  ami_users       = ["730335483982","851725439286"]
  ami_regions     = [
                      "us-east-1",
                      "ap-southeast-1"
                    ]
}

# what to install, configure and file to copy/execute
build {
  name = "hq-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "file" {
  source = "provisioner.sh"
  destination = "/tmp/provisioner.sh"
}

# https://www.packer.io/docs/provisioners/shell 
provisioner "shell" {
    inline = ["chmod a+x /tmp/provisioner.sh"]
  }

  provisioner "shell" {
    inline = ["/tmp/provisioner.sh"]
  }
}
